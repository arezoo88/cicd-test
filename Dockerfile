FROM python:3.8-slim-buster
ENV PYTHONUNBUFFERED=1

RUN mkdir /cicdpr
WORKDIR /cicdpr
COPY . /cicdpr

RUN pip install --upgrade pip
RUN pip3 install -r requirements.txt
RUN python manage.py migrate

CMD ["python3","manage.py","runserver","0.0.0.0:80"]
